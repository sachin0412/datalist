const express = require('express');
const  app = express();
const datas = require('./data');

const verify = require('./token');

app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header(
        "Access-Control-Allow-Headers",
        "Origin, X-Requested-with, Content-Type, Accept, Authorization, auth-token"
    );
    if (req.method === 'OPTIONS') {
        res.header(
            'Access-Control-Allow-Methods', 'PUT , POST, PATCH, DELETE, GET'
        )
        return res.status(200).json({})
    }
    next();
})

app.get('/datas', verify, (req, res) => {                                                                                                                                                                                                                     
    res.send(datas)                                                                                                                                                                                                                                                                                                                                                                            
})                                                                                                                                                                      

app.get('/datas/:id',(req, res) => {
    const data = datas.find(c => c.id === parseInt(req.params.id));
    if (!data) res.status(404).send('The data with the given ID was not found.')
    res.send(data)
})

// PORT 
const port = process.env.PORT || 7000;
app.listen(port, () => console.log(`Listening on port ${port}...`))
